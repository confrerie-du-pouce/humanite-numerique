from unite import Unite
import sys
import json


def openFile(file):
    file = open(file, 'r')
    lines = file.readlines()
    for i in range(len(lines)):
        lines[i] = lines[i].strip().split(',')
    unites = []
    for i in lines:
        unite = Unite(i[0], i[1], i[2], i[4], i[5], i[3])
        unites += [unite]
    return unites


def recallPrecisionFscoreStrict(references, tests):
    correctFound = 0
    for i in references:
        for j in tests:
            if (i.nomCategorie == j.nomCategorie) and (i.startOffset == j.startOffset) and (i.endOffset == j.endOffset):
                correctFound += 1
    res = {}
    res["recall"] = correctFound / len(references)
    res["precision"] = correctFound / len(tests)
    res["fscore"] = 2 * ((res["precision"] * res["recall"]) / (res["precision"] + res["recall"]))
    return res


def recallPrecisionFscoreNonStrict(references, tests):
    correctFound = 0
    for i in references:
        for j in tests:
            if (i.startOffset == j.startOffset) and (i.endOffset == j.endOffset):
                if i.nomCategorie == j.nomCategorie:
                    correctFound += 1
                else:
                    correctFound += 0.5
    res = {}
    res["recall"] = correctFound / len(references)
    res["precision"] = correctFound / len(tests)
    res["fscore"] = 2 * ((res["precision"] * res["recall"]) / (res["precision"] + res["recall"]))
    return res


def saveFile(tosave, name):
    with open('./resultatValidite/' + name + '.json', 'w') as jsonFile:
        json.dump(tosave, jsonFile)


name = str(sys.argv[1])
ref = openFile('./corpusTestValidite/reference.txt')
file = openFile('./corpusTestValidite/' + name + '.txt')

s = recallPrecisionFscoreStrict(ref, file)
ns = recallPrecisionFscoreNonStrict(ref, file)

res = { 'strict': s, 'non strict': ns}

saveFile(res, name)


